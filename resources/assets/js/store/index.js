import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        todos: [
            { body: 'Go to the store', done: false },
            { body: 'Ride on trainer', done: false },
            { body: 'bunny cages', done: false },
            { body: 'Make baby', done: true },
        ]
    },
    
    mutations: {
        addTodo({ todos }, body) {
            todos.push({
                body,
                done: false
            });
        },
        
        completeAll ({ todos }) {
            todos.forEach(todo => todo.done = true);
        },
        
        toggleTodo(state, todo) {
            todo.done = ! todo.done;
        },
        
        deleteTodo({todos}, todo) {
            todos.splice(todos.indexOf(todo), 1);
        }
    }
});
